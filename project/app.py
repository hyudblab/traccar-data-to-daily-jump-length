from typing import Tuple
from sqlalchemy import create_engine
import pandas as pd
from pandas.core.frame import DataFrame
from skmob.core.trajectorydataframe import TrajDataFrame
from skmob.measures.individual import jump_lengths


def get_all_user_positions():
    pg_url = "postgresql://postgres:1234@pg/traccar"
    engine = create_engine(pg_url)
    user_query_sql = """
        select tp.*, td.uniqueid from tc_positions tp, tc_devices td
          where tp.deviceid = td.id 
            and (td.attributes::jsonb ->> 'volunteerName')::text != all(ARRAY['null', 'gs', '테스트'])
            and td.name not like ('@%%') 
            and td.name not like ('#%%')
        ORDER BY tp.fixtime
    """
    df = pd.read_sql(user_query_sql, engine)
    return df


def make_tdf(df: DataFrame) -> Tuple[TrajDataFrame, DataFrame]:
    full_tdf = TrajDataFrame(df, latitude="latitude", longitude="longitude", datetime="fixtime", user_id="uniqueid")
    cut_tdf = full_tdf.drop(["protocol", "deviceid", "servertime", "valid", "speed", "course", "network"], axis=1)
    return full_tdf, cut_tdf


def add_jump_lengths_columns(df: DataFrame):
    jl_df = jump_lengths(df)
    dist_in_km = [0]
    dist_in_km.extend(jl_df["jump_lengths"].iloc[0])
    dist_df = DataFrame({"dist": dist_in_km})
    result = pd.concat([df.reset_index(), dist_df], axis=1)
    return result


def daily_jump_length(df: DataFrame):
    dists_df = df.groupby(df.datetime.dt.date).agg({"dist": "sum", "uid": "first"}).reset_index()
    dists_df.index = dists_df.index + 1
    dists_df = dists_df.reset_index().rename(columns={"index": "day"})
    dists_df["dayOfWeek"] = pd.to_datetime(dists_df["datetime"]).dt.day_name()
    return dists_df


def write_result(df: DataFrame):
    uid = df["uid"].iloc[0]
    print("filename", uid)
    df.to_csv(f"results/{uid}.csv", index=False)


def main():
    df = get_all_user_positions()
    _, tdf = make_tdf(df)
    users = list(tdf["uid"].drop_duplicates())
    print(len(users))
    print(users)
    for user in users:
        print("Processing UID:", user)
        user_tdf = tdf[tdf["uid"] == user]
        user_tdf_with_jump_lengths = add_jump_lengths_columns(user_tdf)
        result = daily_jump_length(user_tdf_with_jump_lengths)
        write_result(result)


if __name__ == "__main__":
    main()
